from MASTERMIND_GAME import MasterMind as m


def test_mastermind():
    """
    1. Try different answers such as 99999, 0, 53030
    2. What if the input has 10 digits?
    3. What if the input is not a number?

    """
    #Testing for 5-Digit Number:
    test_1 = m(5)
    test_1.secret_num = 12598
    
    guess_output = test_1.guess(99999)
    expected_output = {"position matched": 1, "digits matched": 1}
    assert guess_output == expected_output

    guess_output = test_1.guess(10080)
    expected_output = {"position matched": 1, "digits matched": 2}
    assert guess_output == expected_output

    guess_output = test_1.guess("FOOO")
    expected_output = 'Not a Valid Number'
    assert guess_output == expected_output
    
    guess_output = test_1.guess(0)
    expected_output = 'Not a Valid Number'
    assert guess_output == expected_output
    
    guess_output = test_1.guess(784965418918919999)
    expected_output = 'Not a Valid Number'
    assert guess_output == expected_output

    #Testing for 3-Digit Number:
    test_2 = m(3)
    test_2.secret_num = 852
    
    guess_output = test_2.guess(456)
    expected_output = {"position matched": 1, "digits matched": 1}
    assert guess_output == expected_output

    guess_output = test_2.guess(826)
    expected_output = {"position matched": 1, "digits matched": 2}
    assert guess_output == expected_output
    
    guess_output = test_2.guess('SPAMS')
    expected_output = 'Not a Valid Number'
    assert guess_output == expected_output    

    print("==> Test Passed <==")

test_mastermind()

