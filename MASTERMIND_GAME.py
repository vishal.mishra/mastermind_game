class MasterMind:

    def __init__(self, number_of_digits):
        import random
        self.number_of_digits = number_of_digits
        self.secret_num = random.randint(10 ** (self.number_of_digits - 1), 10 ** self.number_of_digits - 1)

    def guess(self, user_number):

        try:
            user_number = int(user_number)
            assert self.number_of_digits == len(str(user_number))
        except:
            return "Not a Valid Number"
        digits_matched = 0
        position_matched = 0
        self.secret_num_cp = self.secret_num
        stringed_number = list(str(user_number))
        self.secret_num_cp = list(str(self.secret_num_cp))
        for i in range(self.number_of_digits):
            if stringed_number[i] == self.secret_num_cp[i]:
                position_matched += 1

        for i in stringed_number:
            if i in self.secret_num_cp:
                digits_matched += 1
                self.secret_num_cp.remove(i)
        return {'position matched': position_matched, 'digits matched': digits_matched}

    def reset(self):
        user_request = input("Do wanna Try Again! (y/n): ")
        if user_request == 'y':
            return True
        else:
            return False


def main():
    reset = True
    while reset:
        attempt = 0
        number_of_digits = int(input("How many Digits would you like to play for : "))
        play = MasterMind(number_of_digits)

        while attempt <= 10:
            if attempt == 10:
                print("\nTry Again! with better guessing skills :D \n")
                reset = play.reset()
                break
            print("---------------------------------------------------")

            user_number = input("\nEnter a {}-Digit Number Of your Choice: ".format(number_of_digits))
            guess_output = play.guess(user_number)
            if isinstance(guess_output, str):
                print(guess_output)

            else:
                user_number = int(user_number)
                if user_number > play.secret_num:
                    print("\nPretty High Guess!, Lower the value of your Number\n")
                elif user_number == play.secret_num:
                    print("\nBull's Eye! That's THE SECRET NUMBER : {}\n".format(play.secret_num))
                    reset = play.reset()
                    break
                else:
                    print("\nPretty Low Number, Raise your Number!\n")

                print(f"Position Matched : {guess_output['position matched']}, Digits Matched : {guess_output['digits matched']}")
                attempt += 1
                print(f"\nAttempts remaining : {10 - attempt}\n")


if __name__ == "__main__":
    main()
